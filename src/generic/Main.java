package generic;

import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        PokojRejestracja<Adult> generyczna = new PokojRejestracja<>();
//        generyczna.

//        List<Double> wynik = collect("abc");
    }

    public static <T> List<T> collect(T... elements){
        List<T> list = new LinkedList<>();
        for (T element: elements) {
            list.add(element);
        }
        return list;
    }
}
