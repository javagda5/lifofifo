package priorityQueue;

public class Main {
    public static void main(String[] args) {
        MyQueue<String> queue = new MyQueue<>();
        queue.push("a", 8);
        queue.push("b", 6);
        queue.push("c", 4);
        queue.push("d", 2);

        queue.printAll();

        queue.push("x", 7);
        queue.push("z", 6);

        System.out.println();
        queue.printAll();

        int b = 4;
        int a = 2;
        int c = 1;
        int wynik = b * b - 4 * a * c;

        System.out.println(wynik);
    }
}
