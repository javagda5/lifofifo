package priorityQueue;

import java.util.LinkedList;
import java.util.List;

public class MyQueue<T> {
    private List<QueueElement<T>> elements = new LinkedList<>();

    public void push(T data, int priority) {
        QueueElement<T> newElement = new QueueElement<>(data, priority);
        for (int i = 0; i < elements.size(); i++) {
            if (elements.get(i).getPriority() < newElement.getPriority()) {
                elements.add(i, newElement);
                return; // konczymy tuz po wstawieniu (po co isc dalej przez kolekcje)
            }
        }
        elements.add(newElement);
    }

    public void printAll() {
        for (QueueElement<T> element : elements) {
            System.out.println(element.getValue());
        }
    }

    public T poll() {
        if (elements.size() == 0) {
            return null;
        }

        QueueElement<T> valueToReturn = elements.get(0);    // bierzemy 1 element kolejki
        elements.remove(0);                 // usuwamy go

        return valueToReturn.getValue();        // zwracamy go
    }

}
