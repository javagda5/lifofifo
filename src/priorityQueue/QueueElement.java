package priorityQueue;

public class QueueElement<T> {
    private T value;        // element kolejki posiada wartość
    private int priority;   // element posiada priorytet

    public QueueElement(T value, int priority) {
        this.value = value;
        this.priority = priority;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}
