package lifo;

import java.util.LinkedList;
import java.util.List;

public class LIFOStructure<Type> {

    private List<Type> collection = new LinkedList<>();

    // umieszczanie
    public void push(Type data) {
        collection.add(data); // dodaje na koniec
    }

    // podglądanie następnego elementu
    public Type peek() {
        if (collection.size() == 0) {
            return null;
        }
        return collection.get(collection.size() - 1); // zwrócenie ostatniego elementu (bez usuniecia)
    }

    // pobierz ostatni (ostatnio dodany bo lifo) element
    public Type pop() {
        if (collection.size() == 0) {
            return null;
        }
        Type elementZwracany = collection.get(collection.size() - 1); // przypisuje do zmiennej
        collection.remove(collection.size() - 1); // usuwam ostatni element
        return elementZwracany; // zwracam go
    }

    public int size(){
        return collection.size();
    }
}
