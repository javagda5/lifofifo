package lifo;

public class LIFOTest {
    public static void main(String[] args) {
        LIFOStructure structure = new LIFOStructure();
        structure.push(1);
        structure.push("abra");
        structure.push("cadabra");
        structure.push("bla");
        structure.push(3);
        structure.push("abra");

        while(structure.size() > 0){
            System.out.println(structure.pop());
        }
    }
}
