package lifo;

import java.util.LinkedHashSet;
import java.util.Set;

public class LIFOSet {

    private Set<Object> collection = new LinkedHashSet<>();

    // umieszczanie
    public void push(Object data) {
        collection.add(data); // dodaje na koniec
    }

    // podglądanie następnego elementu
    public Object peek() {
        if (collection.size() == 0) {
            return null;
        }

        Object wartoscZwracana = null;
        for (Object wartoscWKolekcji : collection) {
            wartoscZwracana = wartoscWKolekcji;
        }
        // po przejsciu przez kolekcję (iterując) ostatni element będzie
        // ostatnim elementem zbioru (dodanym)

        return wartoscZwracana; // zwrócenie ostatniego elementu (bez usuniecia)
    }

    // pobierz ostatni (ostatnio dodany bo lifo) element
    public Object pop() {
        if (collection.size() == 0) {
            return null;
        }

        Object wartoscZwracana = null;
        for (Object wartoscWKolekcji : collection) {
            wartoscZwracana = wartoscWKolekcji;
        }

        collection.remove(wartoscZwracana);

        return wartoscZwracana; // zwracam go
    }

    public int size(){
        return collection.size();
    }
}