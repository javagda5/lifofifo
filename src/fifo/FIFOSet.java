package fifo;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class FIFOSet<T> {

    private Set<T> collection = new LinkedHashSet<>();

    // umieszczanie
    public void push(T data) {
        collection.add(data); // dodaje na koniec
    }

    // podglądanie następnego elementu
    public T peek() {
        // wystarczy ze zwrocimy pierwsza wartosc w kolekcji (1 element)
        for (T wartoscWKolekcji : collection) {
            return wartoscWKolekcji;
        }
        return null;
    }

    // pobierz ostatni (ostatnio dodany bo lifo) element
    public T pop() {

        Iterator<T> it = collection.iterator();
        while(it.hasNext()){
//            Object wartosc = it.next(); // boxing
            T wartosc = it.next(); // boxing
//            collection.remove(wartosc); - nieeeeeeeeeeeee
            it.remove(); // - taaaaaaaaak!
            return wartosc;
        }

        for (T wartoscWKolekcji : collection) {
            collection.remove(wartoscWKolekcji); // usuniecie
            return wartoscWKolekcji;
        }

        return null;
    }

    public int size() {
        return collection.size();
    }
}