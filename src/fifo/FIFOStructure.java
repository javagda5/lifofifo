package fifo;

import java.util.LinkedList;
import java.util.List;

public class FIFOStructure {

    private List<Object> collection = new LinkedList<>();

    // umieszczanie
    public void push(Object data) {
        collection.add(data); // dodaje na koniec
    }

    // podglądanie następnego elementu
    public Object peek() {
        if (collection.size() == 0) {
            return null;
        }
        return collection.get(0); // zwrócenie ostatniego elementu (bez usuniecia)
    }

    // pobierz pierwszy( 0 ) element
    public Object pop() {
        if (collection.size() == 0) {
            return null;
        }
        Object elementZwracany = collection.get(0); // przypisuje do zmiennej
        collection.remove(0); // usuwam ostatni element
        return elementZwracany; // zwracam go
    }

    public int size(){
        return collection.size();
    }
}
